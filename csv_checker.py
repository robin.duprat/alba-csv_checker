#!/usr/bin/env python3

import sys
import csv


################################################################################


# Checker functions

# Sub-functions
def check_fragrance_attribute(value, is_joined=False, product_type=None):
    # Alphbetical order
    #fragrance_list = \
    #    [ "Ambiance Chalet", "Ambre", "Aqua Sancta", "Bellissima", "Blanc d’Arôme", "Bleu Violette",
    #      "Blue Lagon", "Bois de Santal", "Bois Précieux", "Brun d’épices", "Bulles Pétillantes", "Byzance", "Cachemire & Soie",
    #      "Cade", "Camélia", "Cannelle", "Cannelle Mandarine", "Caramel Beurre Salé", "Cèdre", "Cerisier en Fleurs", "Chant d’Orient",
    #      "Chocolat Orange", "Citronnelle Géranium", "Cœur de Châtaigne", "Conte de Fleurs", "Coton & Lin", "Cuir de Russie",
    #      "Cyprès de Provence", "Désir Noir", "Diamant Blanc", "Diamant Noir", "Eau d’Argent", "Eau d’Oranger", "Eau de Cologne",
    #      "Eau Impériale", "Epices et à", "Exquise Poudre de Riz", "Feuille d’Or", "Feuille de Bambou", "Flan’elle Haute Couture",
    #      "Fleurs d’Hibiscus", "Fleur d’O", "Fleur de Cactus", "Fleur de Frangipanier", "Fleur de Lotus", "Fleurs de Baobab",
    #      "Fleurs de Monoï", "Gadjo", "Gentil Coquelicot", "Grain de Beauté", "Grand Cru", "Héliotrope", "Immortelle", "Indiennes",
    #      "Instinct", "Ivoire", "Jasmin des Délices", "L’Eau de Cédrat", "L’Eau de Raphaël", "L’Eau des Montagnes", "L’Eau Turquoise",
    #      "L’Eveil du Monde", "La Dolce Via", "Lait de Coco", "Lait de Figue", "Lavande", "Melon Concombre & Basilic", "Mojito à Cuba",
    #      "Lemon Zest", "Les Fleurs du Mâle", "Lily Rose", "Lys Royal", "Magnolias For Ever", "Mano à Mano", "Matînes", "Mûre Sauvage",
    #      "Murmure", "Orange Tonic", "Pain d’Epices", "Pamplemousse", "Pamplemousse Rose", "Paris I Love You", "Paris mon Amour",
    #      "Parfum des Merveilles", "Patchouli", "Patchouli Blanc", "Patchouli Vanille", "Patio", "Pêche de Vigne", "Petit Biscuit",
    #      "Pois de Senteur", "Pomme d’Amour", "Poudre Divine", "Promenons Nous", "Pur Lin", "Reine de Miel", "Rose Pourpre",
    #      "Rose Pudding", "Rouge Désir", "Sauvage le Parfum", "Sorbet Fraise des Bois", "Sublime Tonka", "Sur un Nuage",
    #      "Thé à la Menthe", "Thé à la Rose", "Thé de Chine", "Thé de la Mer", "Thé Frangipane", "Thé Noir Mangue", "Thé Vert",
    #      "Une Nuit à la Havane", "Vanille des Iles", "Vanille Verveine", "Vent Vert", "Verveine Extra", "Vin Chaud Épicé", "Zinzoline"]
    # Perfum list order (for 'variable' checks)
    fragrance_list = \
        [ "Thé Noir Mangue", "Désir Noir", "Lavande", "Thé de la Mer", "Cèdre", "Thé Frangipane", "Pamplemousse", "Lys Royal",
          "Diamant Blanc", "Lemon Zest", "Thé Vert", "Pur Lin", "Eau de Cologne", "Pois de Senteur", "Lily Rose", "Vanille Verveine",
          "Thé à la Rose", "Lait de Figue", "Cannelle", "Bleu Violette", "Blanc d’Arôme", "Diamant Noir", "Brun d’épices", "Vent Vert",
          "Gadjo", "Fleur de Lotus", "Ivoire", "Byzance", "Héliotrope", "Epices et à", "Reine de Miel", "Caramel Beurre Salé",
          "Chant d’Orient", "Conte de Fleurs", "Murmure", "Mano à Mano", "Fleurs de Monoï", "Camélia", "Cannelle Mandarine",
          "Eau d’Argent", "Chocolat Orange", "Grand Cru", "Ambre", "Bellissima", "Les Fleurs du Mâle", "Indiennes", "Instinct",
          "Matînes", "Cuir de Russie", "Petit Biscuit", "Cade", "Poudre Divine", "Patio", "Zinzoline", "Rouge Désir", "Sur un Nuage",
          "Feuille de Bambou", "Fleur d’O", "Patchouli", "Mojito à Cuba", "Patchouli Blanc", "Fleur de Cactus", "Sauvage le Parfum",
          "Promenons Nous", "Bulles Pétillantes", "Rose Pourpre", "Lait de Coco", "Melon Concombre & Basilic", "Coton & Lin",
          "Rose Pudding", "Orange Tonic", "Verveine Extra", "Blue Lagon", "Une Nuit à la Havane", "Bois Précieux", "Sublime Tonka",
          "Fleurs de Baobab", "Sorbet Fraise des Bois", "Eau Impériale", "Eau d’Oranger", "Exquise Poudre de Riz", "Paris mon Amour",
          "Paris I Love You", "Ambiance Chalet", "Vin Chaud Épicé", "Gentil Coquelicot", "Cerisier en Fleurs", "Pamplemousse Rose",
          "Bois de Santal", "Parfum des Merveilles", "Thé à la Menthe", "Jasmin des Délices", "Magnolias For Ever", "L’Eveil du Monde",
          "Feuille d’Or", "Citronnelle Géranium", "Cœur de Châtaigne", "Cachemire & Soie", "Thé de Chine", "Pomme d’Amour",
          "Flan’elle Haute Couture", "Vanille des Iles", "Pain d’Epices", "Cyprès de Provence", "Mûre Sauvage", "L’Eau des Montagnes",
          "L’Eau de Raphaël", "Grain de Beauté", "L’Eau de Cédrat", "Fleur de Frangipanier", "Immortelle", "Aqua Sancta",
          "Fleurs d’Hibiscus", "L’Eau Turquoise", "La Dolce Via", "Patchouli Vanille", "Pêche de Vigne" ]

    # Fragrance EDT
    edt_fragrance_dict = \
        { "03": "Lavande",
          "10": "Eau de citron",
          "13": "Eau provençale",
          "15": "Lily rose",
          "17": "Pétales de roses",
          "18": "Lait de figue",
          "23": "Brun d’épices",
          "24": "Vent du matin",
          "25": "Gadjo",
          "27": "Musc blanc",
          "33": "Chant d’orient",
          "35": "Sucre d’orge",
          "36": "Mano a mano",
          "38": "Camélia blanc",
          "58": "Fleurs d’o",
          "59": "Patchouli",
          "71": "Eau d’orange",
          "72": "Eau de verveine",
          "82": "O mon amour",
          "86": "Gentil coquelicot",
          "95": "Feuille d’or",
          "98": "Cachemire et soie",
          "102": "Vanilles des Iles",
          "105": "Mure sauvage",
          "116": "Patchouli vanille" }

    # Fragrance PDB huileuses
    pdbh_fragrance_dict = \
        { "03": "Lavande",
          "04": "Thé de la mer",
          "17": "Thé à la rose",
          "21": "Blanc d’arôme",
          "37": "Fleur de monoï",
          "67": "Lait de coco",
          "72": "Verveine extra",
          "78": "Fraise des bois",
          "100": "Pomme d’amour",
          "102": "Vanille bourbon",
          "202": "Pêche gourmande",
          "203": "Fruit de la passion" }

    # Fragrance PDB effervescentes
    pdbe_fragrance_dict = \
        { "01": "Mangue",
          "07": "Pamplemousse",
          "15": "Lily rose",
          "20": "Bleu violette",
          "67": "Perles de coco",
          "87": "Cerisier en fleurs",
          "73": "Blue lagon",
          "78": "Fraise des bois",
          "92": "Jasmin des délices",
          "102": "Vanille Bourbon",
          "105": "Framboise",
          "203": "Fruit de la passion" }

    # Fragrance EDP
    edp_fragrance_dict = \
        { "15": "Lily rose",
          "23": "Brun d’épices",
          "24": "L’eau verte",
          "25": "L’eau de Gadji",
          "27": "Musc blanc",
          "33": "Chant d’orient",
          "35": "Murmure",
          "36": "La main parfumée",
          "37": "Fleurs de monoï",
          "38": "Camélia blanc",
          "58": "Fleurs d’o",
          "59": "Patchouli",
          "71": "L’eau d’orange",
          "82": "Paris mon amour",
          "86": "Gentil coquelicot",
          "95": "Feuille d’or",
          "98": "Cachemire & soie",
          "107": "L’eau de Raphaël" }

    # Fragrance Savon
    soap_fragrance_dict = \
        { "11": "Thé vert",
          "15": "Lily rose",
          "16": "Vanille Verveine",
          "23": "Brun d’épices",
          "37": "Fleur de monoï",
          "38": "Camélia blanc",
          "69": "Coton & lin",
          "72": "Verveine extra",
          "78": "Fraise des bois",
          "80": "Eau d’oranger",
          "86": "Gentil coquelicot",
          "82": "Paris mon amour" }

    if product_type == "Fragrance EDT":
        fragrance_list = {k: v for k, v in sorted(edt_fragrance_dict.items(), key=lambda x: x[1])}
        fragrance_list = fragrance_list.values()
    elif product_type == "Fragrance EDP":
        fragrance_list = {k: v for k, v in sorted(edp_fragrance_dict.items(), key=lambda x: x[1])}
        fragrance_list = fragrance_list.values()
    elif product_type == "Fragrance PDB huileuses":
        fragrance_list = {k: v for k, v in sorted(pdbh_fragrance_dict.items(), key=lambda x: x[1])}
        fragrance_list = fragrance_list.values()
    elif product_type == "Fragrance PDB effervescentes":
        fragrance_list = {k: v for k, v in sorted(pdbe_fragrance_dict.items(), key=lambda x: x[1])}
        fragrance_list = fragrance_list.values()
    elif product_type == "Fragrance Savon":
        fragrance_list = {k: v for k, v in sorted(soap_fragrance_dict.items(), key=lambda x: x[1])}
        fragrance_list = fragrance_list.values()

    if is_joined:
        if value == ",".join(fragrance_list):
            return True
        else:
            return False

    return value in fragrance_list


# Function to retrieve wanted column from rows array
def get_col(col_name, rows):
    CSV_COL= dict()
    CSV_COL["id"] = 0  # A
    CSV_COL["ugs"] = 1  # B
    CSV_COL["name"] = 2  # C
    CSV_COL["parent"] = 3  # D
    CSV_COL["type"] = 4  # E
    CSV_COL["images"] = 5  # F
    CSV_COL["description"] = 6  # G
    CSV_COL["short_descr"] = 7  # H
    CSV_COL["published"] = 8  # I
    CSV_COL["position"] = 9  # J
    CSV_COL["regular_price"] = 10  # K
    CSV_COL["pro_price"] = 11  # L
    CSV_COL["sale_price"] = 12  # M
    CSV_COL["attr1_name"] = 13  # N
    CSV_COL["attr1_value"] = 14  # O
    CSV_COL["attr1_visible"] = 15  # P
    CSV_COL["attr1_global"] = 16  # Q
    CSV_COL["attr2_name"] = 17  # R
    CSV_COL["attr2_value"] = 18  # S
    CSV_COL["attr2_visible"] = 19  # T
    CSV_COL["attr2_global"] = 20  # U
    CSV_COL["attr3_name"] = 21  # V
    CSV_COL["attr3_value"] = 22  # W
    CSV_COL["attr3_visible"] = 23  # X
    CSV_COL["attr3_global"] = 24  # Y
    CSV_COL["tax_status"] = 25  # Z
    CSV_COL["tax_class"] = 26  # AA
    CSV_COL["in_stock"] = 27  # AB
    CSV_COL["stock"] = 28  # AC
    CSV_COL["backorders_allowed"] = 29  # AD
    CSV_COL["lenght"] = 30  # AE
    CSV_COL["height"] = 31  # AF
    CSV_COL["width"] = 32  # AG
    CSV_COL["weight"] = 33  # AH
    CSV_COL["allow_customer_review"] = 34  # AI
    CSV_COL["categories"] = 35  # AJ
    CSV_COL["tags"] = 36  # AK
    CSV_COL["meta_wpcom_is_markdown"] = 37  # AL

    return [elt[CSV_COL[col_name]] for elt in rows]

# ID
def check_id(rows, filename):
    id_array = get_col("id", rows)
    error_str = "{filename}:{line} ID({id}) is not empty"
    for idx, id_elt in enumerate(id_array):
        check_(test=len(id_elt) == 0,
               bad_text=error_str.format(filename=filename, line=(idx+2), id=id_elt))

# UGS
def check_ugs(rows, filename):
    ugs_array = get_col("ugs", rows)
    _s = set(ugs_array)
    if len(ugs_array) == len(_s):
        return # Element are unique in the array, quit here
    error_str = "{filename}:{line} UGS({ugs}) is not unique"
    # Build not unique elt array
    ugs_bad_array = list({x for x in ugs_array if ugs_array.count(x) > 1})
    # Loop over all element and declare which one are not unique

    for idx, ugs_elt in enumerate(ugs_array):
        c = check_(test=not ugs_elt in ugs_bad_array,
                   bad_text=error_str.format(filename=filename, line=(idx+2), ugs=ugs_elt))

# Name

# Parent
def check_parent(rows, filename):
    parent_array = get_col("parent", rows)
    ugs_array = get_col("ugs", rows)
    error_str = "{filename}:{line} Parent({parent}) does not exist in UGS column"
    for idx, parent_elt in enumerate(parent_array):
            check_(test=(len(parent_elt) == 0) or (parent_elt in ugs_array),
                   bad_text=error_str.format(filename=filename, line=(idx+2), parent=parent_elt))

# Type
def check_type(rows, filename):
    type_array = get_col("type", rows)
    parent_array = get_col("parent", rows)
    type_error_str = "{filename}:{line} Type({type}) is not valid"
    parent_error_str = "{filename}:{line} Type({type}) has not valid Parent({parent})"
    for idx, (type_elt, parent_elt) in enumerate(zip(type_array, parent_array)):
            c = check_(test=(type_elt in ["Simple", "Variable", "Variation"]),
                       bad_text=type_error_str
                        .format(filename=filename, line=(idx+2), type=type_elt))
            if not c:
                continue
            check_(test=(type_elt == "Simple" and len(parent_elt) == 0) or
                        (type_elt == "Variable" and len(parent_elt) == 0) or
                        (type_elt == "Variation" and len(parent_elt) != 0),
                   bad_text=parent_error_str
                    .format(filename=filename, line=(idx+2), type=type_elt, parent=parent_elt))

# Image

# DESCRIPTION

# DESCRIPTION COURTE

# Published
def check_published(rows, filename):
    published_array = get_col("published", rows)
    error_str = "{filename}:{line} Published({published}) value is not valid"
    for idx, published_elt in enumerate(published_array):
         check_(test=is_binary(published_elt),
                bad_text=error_str
                 .format(filename=filename, line=(idx+2), published=published_elt))

# Position
def check_position(rows, filename):
    position_array = get_col("position", rows)
    type_array = get_col("type", rows)
    error_range_str = "{filename}:{line} Position({position}) value is not in range[0:255]"
    error_type_str = "{filename}:{line} Position({position}) value has no effect on Variation"
    for idx, position_elt in enumerate(position_array):
        _test=(type_array[idx] == "Variation") or (position_elt in [str(x) for x in range(0,255)])
        check_(test=_test,
               bad_text=error_range_str
                .format(filename=filename, line=(idx+2), position=position_elt))

        _test=(type_array[idx] != "Variation") or (len(position_elt) == 0 and not position_elt.isspace())
        check_(test=_test,
               bad_text=error_type_str
                .format(filename=filename, line=(idx+2), position=position_elt,
                        type=type_array[idx]))


# Regular Price
def check_regular_price(rows, filename):
    price_array = get_col("regular_price", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Regular Price({price}) is not valid"
    for idx, (price_elt, type_elt) in enumerate(zip(price_array,type_array)):
        _test=(type_elt.lower() == "variable" and price_elt == "") or \
               price_elt.replace(" ", "").replace("€", "").replace(".", "").replace(",", "").isdecimal()
        check_(test=_test,
               bad_text=error_str
                .format(filename=filename, line=(idx+2), price=price_elt))
# PRO HT Price
def check_pro_price(rows, filename):
    price_array = get_col("pro_price", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Pro Price({price}) is not valid"
    for idx, (price_elt, type_elt) in enumerate(zip(price_array,type_array)):
        _test=(type_elt.lower() == "variable" and price_elt == "") or \
               price_elt.replace(" ", "").replace("€", "").replace(".", "").replace(",", "").isdecimal()
        check_(test=_test,
               bad_text=error_str
                .format(filename=filename, line=(idx+2), price=price_elt))
# Sale Price
# Attribute 1 name
# Attribute 1 value(s)
def check_attribute_1_value(rows, filename):
    ugs_array = get_col("ugs", rows)
    parent_array = get_col("parent", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr1_name", rows)
    value_attribute_array = get_col("attr1_value", rows)
    error_str = "{filename}:{line} Attribute 1 {name}=\"{value}\" is not valid"
    variable_list = []
    for idx, (name_attribute_elt, value_attribute_elt, type_elt) \
    in enumerate(zip(name_attribute_array, value_attribute_array, type_array)):
        if (type_elt.lower() == "variable") and (name_attribute_elt[0:10] == "Fragrance"):
            variable_list += [ugs_array[idx]]
            check_(test=check_fragrance_attribute(value_attribute_elt, is_joined=True, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt[:16]+"[...]"))
        elif (type_elt.lower() == "variation") and (ugs_array[idx] in variable_list):
            check_(test=name_attribute_elt[0:10] == "Fragrance",
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt[0:10] == "Fragrance"):
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt != ""):
            check_(test=name_attribute_elt.split(" ")[0] in ["Fragrance", "Contenance", "Couleur"],
                   bad_text=error_str.replace("\"","").format(filename=filename, line=(idx+2),
                                                              name="\"" + name_attribute_elt + "\"",
                                                              value=value_attribute_elt))
        else:
            # Only simple can be empty (in attribute 1, otherwise why having a Variable/Variation ?)
            check_(test=type_elt.lower() == "simple",
                   bad_text=error_str.replace("\"","").format(filename=filename, line=(idx+2),
                                                              name="\"" + name_attribute_elt + "\"",
                                                              value=value_attribute_elt))


# Attribute 1 visible
def check_attribute_1_visible(rows, filename):
    attribute_array = get_col("attr1_visible", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr1_name", rows)
    value_attribute_array = get_col("attr1_value", rows)
    error_str = "{filename}:{line} Attribute 1 visible({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt) in enumerate(zip(attribute_array, type_array)):
        # Only simple can be empty (in attribute 1, otherwise why having a Variable/Variation ?)
        is_empty_simple = (type_elt.lower() == "simple") and \
                          (name_attribute_array[idx] == "") and \
                          (value_attribute_array[idx] == "") and \
                          (attribute_elt == "")
        check_(test=(is_empty_simple or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))

# Attribute 1 global
def check_attribute_1_global(rows, filename):
    attribute_array = get_col("attr1_global", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr1_name", rows)
    value_attribute_array = get_col("attr1_value", rows)
    error_str = "{filename}:{line} Attribute 1 global({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt)in enumerate(zip(attribute_array, type_array)):
        # Only simple can be empty (in attribute 1, otherwise why having a Variable/Variation ?)
        is_empty_simple = (type_elt.lower() == "simple") and \
                          (name_attribute_array[idx] == "") and \
                          (value_attribute_array[idx] == "") and \
                          (attribute_elt == "")
        check_(test=(is_empty_simple or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))

# Attribute 2 name
# Attribute 2 value(s)
def check_attribute_2_value(rows, filename):
    ugs_array = get_col("ugs", rows)
    parent_array = get_col("parent", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr2_name", rows)
    value_attribute_array = get_col("attr2_value", rows)
    error_str = "{filename}:{line} Attribute 2 {name}=\"{value}\" is not valid"
    variable_list = []
    for idx, (name_attribute_elt, value_attribute_elt, type_elt) in \
     enumerate(zip(name_attribute_array, value_attribute_array, type_array)):
        if (type_elt.lower() == "variable") and (name_attribute_elt[0:10] == "Fragrance"):
            variable_list += [ugs_array[idx]]
            check_(test=check_fragrance_attribute(value_attribute_elt, is_joined=True, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt[:16]+"[...]"))
        elif (type_elt.lower() == "variation") and (ugs_array[idx] in variable_list):
            check_(test=name_attribute_elt[0:10] == "Fragrance",
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt[0:10] == "Fragrance"):
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt != ""):
            check_(test=name_attribute_elt.split(" ")[0] in ["Fragrance", "Contenance", "Couleur"],
                   bad_text=error_str.replace("\"","").format(filename=filename, line=(idx+2),
                                                              name="\"" + name_attribute_elt + "\"",
                                                              value=value_attribute_elt))

# Attribute 2 visible
def check_attribute_2_visible(rows, filename):
    attribute_array = get_col("attr2_visible", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr2_name", rows)
    value_attribute_array = get_col("attr2_value", rows)
    error_str = "{filename}:{line} Attribute 2 visible({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt)in enumerate(zip(attribute_array, type_array)):
        is_empty = (name_attribute_array[idx] == "") and \
                   (value_attribute_array[idx] == "") and \
                   (attribute_elt == "")
        check_(test=(is_empty or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))

# Attribute 2 global
def check_attribute_2_global(rows, filename):
    attribute_array = get_col("attr2_global", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr2_name", rows)
    value_attribute_array = get_col("attr2_value", rows)
    error_str = "{filename}:{line} Attribute 2 global({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt)in enumerate(zip(attribute_array, type_array)):
        is_empty = (name_attribute_array[idx] == "") and \
                   (value_attribute_array[idx] == "") and \
                   (attribute_elt == "")
        check_(test=(is_empty or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))


# Attribute 3 name
# Attribute 3 value(s)
def check_attribute_3_value(rows, filename):
    ugs_array = get_col("ugs", rows)
    parent_array = get_col("parent", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr3_name", rows)
    value_attribute_array = get_col("attr3_value", rows)
    error_str = "{filename}:{line} Attribute 3 {name}=\"{value}\" is not valid"
    variable_list = []
    for idx, (name_attribute_elt, value_attribute_elt, type_elt) in \
     enumerate(zip(name_attribute_array, value_attribute_array, type_array)):
        if (type_elt.lower() == "variable") and (name_attribute_elt[0:10] == "Fragrance"):
            variable_list += [ugs_array[idx]]
            check_(test=check_fragrance_attribute(value_attribute_elt, is_joined=True, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt[:16]+"[...]"))
        elif (type_elt.lower() == "variation") and (ugs_array[idx] in variable_list):
            check_(test=name_attribute_elt[0:10] == "Fragrance",
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt[0:10] == "Fragrance"):
            check_(test=check_fragrance_attribute(value_attribute_elt, product_type=name_attribute_elt),
                   bad_text=error_str.format(filename=filename, line=(idx+2),
                                             name=name_attribute_elt, value=value_attribute_elt))
        elif (name_attribute_elt != ""):
            check_(test=name_attribute_elt.split(" ")[0] in ["Fragrance", "Contenance", "Couleur"],
                   bad_text=error_str.replace("\"","").format(filename=filename, line=(idx+2),
                                                              name="\"" + name_attribute_elt + "\"",
                                                              value=value_attribute_elt))

# DISABLED Attribute 3 visible
def check_attribute_3_visible(rows, filename):
    attribute_array = get_col("attr3_visible", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr3_name", rows)
    value_attribute_array = get_col("attr3_value", rows)
    error_str = "{filename}:{line} Attribute 3 visible({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt)in enumerate(zip(attribute_array, type_array)):
        is_empty = (name_attribute_array[idx] == "") and (attribute_elt == "")
        check_(test=(is_empty or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))

# DISABLED Attribute 3 global
def check_attribute_3_global(rows, filename):
    attribute_array = get_col("attr3_global", rows)
    type_array = get_col("type", rows)
    name_attribute_array = get_col("attr3_name", rows)
    value_attribute_array = get_col("attr3_value", rows)
    error_str = "{filename}:{line} Attribute 3 global({attribute}) value is not valid"
    for idx, (attribute_elt, type_elt)in enumerate(zip(attribute_array, type_array)):
        is_empty = (name_attribute_array[idx] == "") and (attribute_elt == "")
        check_(test=(is_empty or is_binary(attribute_elt)),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), attribute=attribute_elt))

# Tax status
#) Tax class

#) In stock?
def check_in_stock(rows, filename):
    in_stock_array = get_col("in_stock", rows)
    error_str = "{filename}:{line} In stock?({in_stock}) value is not valid"
    for idx, in_stock_elt in enumerate(in_stock_array):
        check_(test=(is_binary(in_stock_elt) or len(in_stock_elt) == 0),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), in_stock=in_stock_elt))

#) STOCK

#) Backorders allowed?
def check_backorders_allowed(rows, filename):
    backorders_allowed_array = get_col("backorders_allowed", rows)
    error_str = "{filename}:{line} Backorders allowed?({backorders_allowed}) value is not valid"
    for idx, backorders_allowed_elt in enumerate(backorders_allowed_array):
        check_(test=(is_binary(backorders_allowed_elt) or len(backorders_allowed_elt) == 0),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), backorders_allowed=backorders_allowed_elt))

#) Longueur (cm)
def check_longueur(rows, filename):
    longueur_array = get_col("lenght", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Longueur=\"{elt}\" should be a number"
    for idx, longueur_array_elt in enumerate(longueur_array):
        check_(test=((type_array[idx].lower() == "variable") or longueur_array_elt.replace(",", "").replace(".", "").isdecimal()),
               bad_text=error_str.format(filename=filename, line=(idx+2), elt=longueur_array_elt))

#) Hauteur (cm)
def check_hauteur(rows, filename):
    hauteur_array = get_col("height", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Hauteur=\"{elt}\" should be a number"
    for idx, hauteur_array_elt in enumerate(hauteur_array):
        check_(test=((type_array[idx].lower() == "variable") or hauteur_array_elt.replace(",", "").replace(".", "").isdecimal()),
               bad_text=error_str.format(filename=filename, line=(idx+2), elt=hauteur_array_elt))

#) Largeur (cm)
def check_largeur(rows, filename):
    largeur_array = get_col("width", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Largeur=\"{elt}\" should be a number"
    for idx, largeur_array_elt in enumerate(largeur_array):
        check_(test=((type_array[idx].lower() == "variable") or largeur_array_elt.replace(",", "").replace(".", "").isdecimal()),
               bad_text=error_str.format(filename=filename, line=(idx+2), elt=largeur_array_elt))

#) Poids (kg)
def check_poids(rows, filename):
    poids_array = get_col("weight", rows)
    type_array = get_col("type", rows)
    error_str = "{filename}:{line} Poids:\"{elt}\" should be a number (without ','/'.')"
    for idx, poids_array_elt in enumerate(poids_array):
        check_(test=((type_array[idx].lower() == "variable") or poids_array_elt.isdecimal()),
               bad_text=error_str.format(filename=filename, line=(idx+2), elt=poids_array_elt))


#) Allow customer reviews?
def check_allow_customer_rev(rows, filename):
    allow_customer_rev_array = get_col("allow_customer_review", rows)
    error_str = "{filename}:{line} Allow customer reviews?({allow_customer_rev}) value is not valid"
    for idx, allow_customer_rev_elt in enumerate(allow_customer_rev_array):
        check_(test=(is_binary(allow_customer_rev_elt) or len(allow_customer_rev_elt) == 0),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), allow_customer_rev=allow_customer_rev_elt))

#) CATEGORIES
def check_categories(rows, filename):
    categories_array = get_col("categories", rows)
    error_str = "{filename}:{line} \"Categories\" should not be empty"
    for idx, categories_array_elt in enumerate(categories_array):
        check_(test=((len(categories_array_elt) != 0) and not categories_array_elt.isspace()),
               bad_text=error_str.format(filename=filename, line=(idx+2)))

#) Tags

#) Meta: _wpcom_is_markdown
def check_meta(rows, filename):
    meta_array = get_col("meta_wpcom_is_markdown", rows)
    error_str = "{filename}:{line} Meta: _wpcom_is_markdown({meta}) value is not valid"
    for idx, meta_elt in enumerate(meta_array):
        check_(test=is_binary(meta_elt),
               bad_text=error_str
                .format(filename=filename, line=(idx+2), meta=meta_elt))



################################################################################
# Internal functions
nb_err=0
def check_(test, bad_text):
    if not test:
        print(bad_text)
        global nb_err
        nb_err+=1
        return False
    return True

def is_binary(elt):
    return elt == "0" or elt == "1"


################################################################################
# Main function
check_list=[
            check_id,                       # Check the 'ID' column
            check_ugs,                      # Check the 'UGS' column
            check_parent,                   # Check the 'Parent' column
            check_type,                     # Check the 'Type' column
            check_published,                # Check the 'Published' column
            check_position,                 # Check the 'Position' column
            check_regular_price,            # Check the 'Regular price' column
            check_pro_price,                # Check the 'Pro HT price' column
            check_attribute_1_value,        # check the 'attribute 1 value'
            check_attribute_1_visible,      # Check the 'Attribute 1 visible'
            check_attribute_1_global,       # Check the 'Attribute 1 global'
            check_attribute_2_value,        # check the 'attribute 1 value'
            check_attribute_2_visible,      # Check the 'Attribute 2 visible'
            check_attribute_2_global,       # Check the 'Attribute 2 global'
            check_attribute_3_value,        # check the 'attribute 3 value'
            check_attribute_3_visible,      # Check the 'Attribute 3 visible'
            check_attribute_3_global,       # Check the 'Attribute 3 global'
            check_in_stock,                 # Check the 'in stock?'
            check_backorders_allowed,       # Check the 'Backorders allowed?'
            check_longueur,                 # Check the 'Longueur'
            check_hauteur,                  # Check the 'Hauteur'
            check_largeur,                  # Check the 'Largeur'
            check_poids,                    # Check the 'Poids'
            check_allow_customer_rev,       # Check the 'Allow customer reviews?'
            check_categories,               # Check the 'Categories'
            check_meta,                     # Check the 'Meta: _wpcom_is_markdown'
            None
           ]

def main():
    # Iter over files given in argument list
    for filename in sys.argv[1:]:
        # Print nicely the filename
        nb_sep = 90 - len(filename)
        sep_s, sep_e = ((nb_sep//2), ((nb_sep//2)+(nb_sep%2)))
        print(("=" * sep_s + " %s " + "=" * sep_e) % filename)
        # Open the file
        file = open(filename)
        # Convert it as csv object
        csvreader = csv.reader(file)
        # Get first row (header)
        header = next(csvreader)
        # Get others row (data)
        rows = [row for row in csvreader]
        # Close the file
        file.close()
        # Run the check list
        for check in check_list:
            if check is not None:
                # Print nicely the test name
                fct_name = check.__name__
                nb_sep = 90 - len(fct_name)
                sep_s, sep_e = ((nb_sep//2), ((nb_sep//2)+(nb_sep%2)))
                print(("-" * sep_s + " %s " + "-" * sep_e) % check.__name__)
                # Execute the test
                check(rows, filename)
        print("\n")

main()
