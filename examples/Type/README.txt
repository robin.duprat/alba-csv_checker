# Check of Type

1) Authorized 'Type' values shall be "Simple", "Variable" or "Variation"

2) Type "Simple" and "Variable" shall have an empty 'Parent'

3) Type "Variation" shall have an NOT empty 'Parent'
