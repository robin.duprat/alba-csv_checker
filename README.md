# Test for ALBA CSV file

## Prerequisite

Python3 installed.
> -> See here [https://www.python.org/downloads/](https://www.python.org/downloads/).

## Examples test

For each column of the csv file a directory exist in examples/ folder.
Inside each one, a README.txt file is present to explain which kind of test is performed.

For example, if you take a look to examples/ID/README.txt, you will see a description of the 'ID' column tests,
with by at least 2 csv file:
- a good one (ok.csv)
- and a BAD one (ko-xxxx.csv) to verify that the csv_checker.py is correctly catching expecting errors

## Run the csw_checker.py

```
$ ./csw_checker.py <my_csv_filename>
```

Replacing ```<my_csv_filename>``` by your own CSV file name


## Run the examples test (bash needed !)

```
$ ./examples.sh
```
